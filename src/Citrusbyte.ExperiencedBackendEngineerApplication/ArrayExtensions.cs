﻿using System;
using System.Collections.Generic;

namespace Citrusbyte.ExperiencedBackendEngineerApplication
{
    public static class ArrayExtensions
    {
        public static int MaxDepthLimit { get; } = 20000;

        public static IEnumerable<int> FlattenIntegers(
            this Array arbitrarilyNestedArrayOfIntegers,
            int? depthLimit = null)
        {
            if (arbitrarilyNestedArrayOfIntegers == null)
                throw new ArgumentNullException(nameof(arbitrarilyNestedArrayOfIntegers));
            depthLimit = depthLimit ?? MaxDepthLimit;
            if (depthLimit <= 0 || depthLimit > MaxDepthLimit)
                throw new ArgumentOutOfRangeException(
                    nameof(depthLimit),
                    $"Value should be in range (0; {MaxDepthLimit}]");
            var currentPosition = new Queue<long>();
            return FlattenIntegersInternal(
                arbitrarilyNestedArrayOfIntegers,
                // to avoid stack overflow
                depthLimit.Value,
                // to get a problematic item address in case of error
                currentPosition,
                // to detect loops in structure
                new HashSet<object>(),
                error => throw new ArrayFlatteningException(error, currentPosition.ToArray()));
        }

        private static IEnumerable<int> FlattenIntegersInternal(
            Array arbitrarilyNestedArray,
            int depthLimit,
            Queue<long> currentPosition,
            ISet<object> visitedArrays,
            Action<ArrayFlatteningError> errorAction)
        {
            if (depthLimit < 0)
            {
                errorAction(ArrayFlatteningError.DepthLimitReached);
                yield break;
            }
            if (arbitrarilyNestedArray.LongLength == 0)
                yield break;
            if (!visitedArrays.Add(arbitrarilyNestedArray))
            {
                errorAction(ArrayFlatteningError.SecondOccuranceOfSameArray);
                yield break;
            }
            for (var i = 0L; i < arbitrarilyNestedArray.LongLength; i++)
            {
                var item = arbitrarilyNestedArray.GetValue(i);
                // avoid populating the queue for ordinary case
                if (item is int integerItem)
                {
                    yield return integerItem;
                    continue;
                }
                currentPosition.Enqueue(i);
                if (item == null)
                {
                    errorAction(ArrayFlatteningError.ValueIsNull);
                    yield break;
                }
                var itemType = item.GetType();
                if (!itemType.IsArray)
                {
                    errorAction(ArrayFlatteningError.ValueOfAWrongType);
                    yield break;
                }
                foreach (var childItem in FlattenIntegersInternal(
                    (Array)item,
                    depthLimit - 1,
                    currentPosition,
                    visitedArrays,
                    errorAction))
                    yield return childItem;
                currentPosition.Dequeue();
            }
        }
    }
}
