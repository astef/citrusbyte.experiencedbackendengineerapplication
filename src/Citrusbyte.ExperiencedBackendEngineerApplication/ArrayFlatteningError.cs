﻿namespace Citrusbyte.ExperiencedBackendEngineerApplication
{
    public enum ArrayFlatteningError
    {
        DepthLimitReached = 0,
        SecondOccuranceOfSameArray,
        ValueIsNull,
        ValueOfAWrongType
    }
}