﻿using System;
using System.Collections.Generic;

namespace Citrusbyte.ExperiencedBackendEngineerApplication
{
    [Serializable]
    public sealed class ArrayFlatteningException : Exception
    {
        internal ArrayFlatteningException(ArrayFlatteningError reason, IReadOnlyList<long> position) :
            base(FormatMessage(reason, position))
        {
            Reason = reason;
            Position = position;
        }

        public ArrayFlatteningError Reason { get; }

        public IReadOnlyList<long> Position { get; }

        private static string FormatMessage(ArrayFlatteningError reason, IEnumerable<long> position) =>
            $"Error occured at position [{string.Join("][", position)}]: {reason}";
    }
}