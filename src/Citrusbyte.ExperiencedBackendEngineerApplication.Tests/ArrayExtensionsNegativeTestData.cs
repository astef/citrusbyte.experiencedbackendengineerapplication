using System;

namespace Citrusbyte.ExperiencedBackendEngineerApplication.Tests
{
    public sealed class ArrayExtensionsNegativeTestData
    {
        public ArrayExtensionsNegativeTestData(
            string uniqueDescription,
            Array input,
            ArrayFlatteningError error,
            long[] errorPosition,
            int? depthLimit = null)
        {
            UniqueDescription = uniqueDescription;
            Input = input;
            DepthLimit = depthLimit;
            Error = error;
            ErrorPosition = errorPosition;
        }
        /// <summary>
        /// Needed for a test case to be uniquely identified by a test runner
        /// </summary>
        public string UniqueDescription { get; }

        public Array Input { get; }

        public int? DepthLimit { get; }

        public ArrayFlatteningError Error { get; }

        public long[] ErrorPosition { get; }

        public override string ToString() => UniqueDescription;
    }
}