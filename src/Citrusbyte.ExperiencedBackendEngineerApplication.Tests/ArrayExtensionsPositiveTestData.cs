using System;

namespace Citrusbyte.ExperiencedBackendEngineerApplication.Tests
{
    public sealed class ArrayExtensionsPositiveTestData
    {
        public ArrayExtensionsPositiveTestData(string uniqueDescription, Array input, int[] expectedResult, int? depthLimit = null)
        {
            UniqueDescription = uniqueDescription;
            Input = input;
            DepthLimit = depthLimit;
            ExpectedResult = expectedResult;
        }

        /// <summary>
        /// Needed for a test case to be uniquely identified by a test runner
        /// </summary>
        public string UniqueDescription { get; }

        public Array Input { get; }

        public int? DepthLimit { get; }

        public int[] ExpectedResult { get; }

        public override string ToString() => UniqueDescription;
    }
}