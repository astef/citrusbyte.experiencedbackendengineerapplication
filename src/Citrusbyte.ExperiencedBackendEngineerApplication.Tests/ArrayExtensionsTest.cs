using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace Citrusbyte.ExperiencedBackendEngineerApplication.Tests
{
    public sealed class ArrayExtensionsTest
    {
        [Theory]
        [MemberData(nameof(GeneratePositiveTestInput))]
        public void TestPositiveCases(ArrayExtensionsPositiveTestData testData)
        {
            testData.Input.FlattenIntegers(testData.DepthLimit).ToArray().Should()
                .BeEquivalentTo(testData.ExpectedResult);
        }

        [Theory]
        [MemberData(nameof(GenerateNegativeTestInput))]
        public void TestNegativeCases(ArrayExtensionsNegativeTestData testData)
        {
            Action actStage = () =>
            {
                foreach (var _ in testData.Input.FlattenIntegers(testData.DepthLimit)) { }
            };
            actStage.Should().Throw<ArrayFlatteningException>()
                .Where(e => e.Reason == testData.Error)
                .Where(e => e.Position.SequenceEqual(testData.ErrorPosition));
        }

        public static IEnumerable<object[]> GeneratePositiveTestInput()
        {
            // it is allowed to have duplicate arrays only if they're empty
            var duplicateEmptyArray = Array.Empty<int>();

            yield return new object[] { new ArrayExtensionsPositiveTestData(
                "Empty array of int",
                Array.Empty<int>(),
                Array.Empty<int>()) };

            yield return new object[] { new ArrayExtensionsPositiveTestData(
                "Two same empty arrays",
                duplicateEmptyArray,
                duplicateEmptyArray) };

            yield return new object[] { new ArrayExtensionsPositiveTestData(
                "Two same empty arrays in a structure",
                new object[] { new[] { new[] { new[] { duplicateEmptyArray } } }, duplicateEmptyArray },
                Array.Empty<int>()) };

            yield return new object[] { new ArrayExtensionsPositiveTestData(
                "Empty array of Array",
                Array.Empty<Array>(),
                Array.Empty<int>()) };

            yield return new object[] { new ArrayExtensionsPositiveTestData(
                "Empty array of string",
                Array.Empty<string>(),
                Array.Empty<int>()) };

            yield return new object[] { new ArrayExtensionsPositiveTestData(
                "Empty array of object",
                Array.Empty<object>(),
                Array.Empty<int>()) };

            yield return new object[] { new ArrayExtensionsPositiveTestData(
                "Array of zero",
                new[] { 0 },
                new[] { 0 }) };

            yield return new object[] { new ArrayExtensionsPositiveTestData(
                "Plain array of 5 elements",
                new[] { 3, 2, 5, 0, 1 },
                new[] { 3, 2, 5, 0, 1 }) };

            yield return new object[] { new ArrayExtensionsPositiveTestData(
                "Plain array with depth limit 1",
                new[] { 42 },
                new[] { 42 },
                1) };

            yield return new object[] { new ArrayExtensionsPositiveTestData(
                "Plain array with empty array at the end",
                new object[] { 1, 2, int.MaxValue, int.MinValue, new string[0] },
                new[] { 1, 2, int.MaxValue, int.MinValue }) };

            yield return new object[] { new ArrayExtensionsPositiveTestData(
                "Plain array with empty array in the beginning",
                new object[] { new string[0], 1, 2, -1, 0 },
                new[] { 1, 2, -1, 0 }) };

            yield return new object[] { new ArrayExtensionsPositiveTestData(
                "Nested array of level 2",
                new object[] { new[] { 1 }, 2, 3, new[] { 4, 5, 6 }, 7, new int[0], 8, 9 },
                new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 }) };

            yield return new object[] { new ArrayExtensionsPositiveTestData(
                "Nested array of level 3",
                new object[]
                {
                    new[] { new[] { 1 } },
                    new[] { 2 },
                    3,
                    new object[] { new[] { 4, 5 }, 6 },
                    7,
                    new object[] { new[] { duplicateEmptyArray }, duplicateEmptyArray },
                    new[] { new[] { new[] { 8 } } },
                    9
                },
                new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 }) };

            yield return new object[] { new ArrayExtensionsPositiveTestData(
                "Nested array of level 4 with depth limit 4",
                new object[]
                {
                    new[] { new[] { new[] { new[] { int.MaxValue } } } },
                },
                new[] { int.MaxValue },
                4) };
        }

        public static IEnumerable<object[]> GenerateNegativeTestInput()
        {
            yield return new object[] { new ArrayExtensionsNegativeTestData(
                "Nested array of level 4 with depth limit 3",
                new object[]
                {
                    new[] { new[] { new[] { new[] { int.MaxValue } } } },
                }, ArrayFlatteningError.DepthLimitReached,
                new long[] { 0, 0, 0, 0},
                3) };

            var duplicateArray = new[] { 0 };
            yield return new object[] { new ArrayExtensionsNegativeTestData(
                "Duplicate array in structure",
                new object[]
                {
                    duplicateArray,
                    new[] { new[] { new[] { new[] { new[] { duplicateArray } } } } },
                }, ArrayFlatteningError.SecondOccuranceOfSameArray,
                new long[] { 1, 0, 0, 0, 0, 0}) };

            yield return new object[] { new ArrayExtensionsNegativeTestData(
                "Null value",
                new object[]
                {
                    new[] { new[] { new[] { new[] { new object[] { 1, null } } } } },
                }, ArrayFlatteningError.ValueIsNull,
                new long[] { 0, 0, 0, 0, 0, 1}) };

            yield return new object[] { new ArrayExtensionsNegativeTestData(
                "Wrong value type",
                new object[]
                {
                    new[] { new[] { new[] { new[] { "" } } } },
                }, ArrayFlatteningError.ValueOfAWrongType,
                new long[] { 0, 0, 0, 0, 0 }) };
        }
    }
}
